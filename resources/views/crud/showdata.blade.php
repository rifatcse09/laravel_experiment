<link href="{{URL :: asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
$('#example').DataTable();
} );
</script>
<div class="row">
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>

        <tr>
            <th>Sl</th>
            <th>Name</th>
            <th>Email</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
    </thead>
       @php $sl = 1;
    @endphp
    @foreach($alldata as $data)
    <tr>
        <th>{{$sl++}}</th>
        <th>{{$data->name}}</th>
        <th>{{$data->email}}</th>
        <th><a href="{{Route('crud.edit',$data->id)}}">Edit</a></th>
        <th>
            {!! Form :: open(['method'=>'Delete','route' =>['crud.destroy',$data->id]]) !!}
         {!! Form :: submit('Delete') !!}
            {!! Form::close() !!}
        </th>

    </tr>
        @endforeach


</table>
    </div>