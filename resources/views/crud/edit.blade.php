
<link href="{{URL :: asset('css/bootstrap.min.css')}}" rel="stylesheet">

<div class="container">

    {!! Form::open(array('route' => ['crud.update',$edit->id],'method' => 'PUT')) !!}

    <div class="form-group">

        {!! Form::label('name', 'Full Name') !!}
        {!! Form::text('name', $value = $edit->name) !!}

    </div>
    <div class="form-group">

        {!! Form::label('email', 'E-Mail Address') !!}
        {!! Form::text('email', $value = $edit->email) !!}

    </div>
    <div class="form-group">

        {!! Form::label('password', 'Password') !!}
        {!! Form::text('password', $value = $edit->password) !!}

    </div>

    <div class="form-group">

        {!! Form::submit('Submit', $attributes = array('class' => 'btn btn-primary')) !!}

        {!! Form::close() !!}
    </div>
</div>