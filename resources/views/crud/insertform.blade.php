
<link href="{{URL :: asset('css/bootstrap.min.css')}}" rel="stylesheet">

<div class="container">
   {!!Form::open(array('crud' => 'crud.store'))!!}
   {!! Form::token() !!}
   
  <div class="form-group">
  
	{!! Form::label('name', 'Full Name') !!}
		{!! Form::text('name') !!}

  </div>
   <div class="form-group">
  
	{!! Form::label('email', 'E-Mail Address') !!}
		{!! Form::text('email') !!}

  </div>
     <div class="form-group">
  
	{!! Form::label('password', 'Password') !!}
		{!! Form::password('password') !!}

  </div>
 
  <div class="form-group">
 
{!! Form::submit('Submit',$attributes = array('class' => 'btn btn-primary')) !!}

{!!Form::close()!!}
</div>