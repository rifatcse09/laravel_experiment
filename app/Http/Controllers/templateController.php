<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class templateController extends Controller
{
    //
    public function template_fun()
    {
    	return view('template');
    }
    public function template_flot()
    {
        return view('flot');
    }
}
