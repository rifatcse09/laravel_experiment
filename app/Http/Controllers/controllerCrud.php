<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model_registraion;

class controllerCrud extends Controller
{
	
	public function index()
	{
		return view('crud.insertform');

	}
	public function store(Request $request){
		$input = $request->all();
	    Model_registraion :: create($input);
		$alldata = Model_registraion :: paginate(105);
		return view('crud.showdata',compact('alldata'));

	}
	public function edit($id)
	{
		$edit = Model_registraion :: findorFail($id);
		return view('crud.edit',compact('edit'));

	}
	public function update(Request $request, $id)
	{
		$input = $request->all();
		$edit= Model_registraion :: findorFail($id);
		$edit->update($input);
		return redirect('crud');

	}

	public function destroy(Request $request, $id)
	{
		//$input = $request->all();
		$edit= Model_registraion :: findorFail($id);
		$edit->delete();
		return redirect('crud');

	}

}
